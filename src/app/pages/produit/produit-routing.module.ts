import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ProduitComponent} from "./produit.component";
import {ProduitDetailComponent} from "./components/produit-detail/produit-detail.component";

const routes: Routes = [
  { path: "", component: ProduitComponent, children: [
      { path: ":id", component: ProduitDetailComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProduitRoutingModule { }
